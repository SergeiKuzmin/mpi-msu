from mpi4py import MPI

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

print('Hello from process {0} out of {1}'.format(rank, numprocs))
