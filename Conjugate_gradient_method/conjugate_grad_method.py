from mpi4py import MPI
import numpy as np
import matplotlib.pyplot as plt
import time

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()


def aux_rcounts_and_displs(N_full, num_process):
    ave, res = divmod(N_full, num_process - 1)
    rcounts = np.empty(num_process, dtype=np.int32)
    displs = np.empty(num_process, dtype=np.int32)
    rcounts[0] = 0
    displs[0] = 0
    for k in range(1, num_process):
        if k < 1 + res:
            rcounts[k] = ave + 1
        else:
            rcounts[k] = ave
        displs[k] = displs[k - 1] + rcounts[k - 1]
    return rcounts, displs


def conjugate_gradient_method(A_pt, b_pt, x_full, N_full):
    p_full = np.zeros(N_full, dtype=np.float64)
    r_full = np.zeros(N_full, dtype=np.float64)
    q_full = np.zeros(N_full, dtype=np.float64)

    for s in range(1, N_full + 1, 1):
        if s == 1:
            r_tmp = np.dot(A_pt.T, np.dot(A_pt, x_full) - b_pt)
            comm.Allreduce([r_tmp, N_full, MPI.DOUBLE], [r_full, N_full, MPI.DOUBLE], op=MPI.SUM)
        else:
            r_full -= q_full / np.dot(p_full, q_full)

        p_full += r_full / np.dot(r_full, r_full)

        q_tmp = np.dot(A_pt.T, np.dot(A_pt, p_full))
        comm.Allreduce([q_tmp, N_full, MPI.DOUBLE], [q_full, N_full, MPI.DOUBLE], op=MPI.SUM)

        x_full -= p_full / np.dot(p_full, q_full)

        # Error calculation
        error = np.zeros(1, dtype=np.float64)

        y_pt = np.dot(A_pt, x_full)
        error_tmp = np.sum(np.abs(y_pt - b_pt) ** 2)

        comm.Allreduce([error_tmp, 1, MPI.DOUBLE], [error, 1, MPI.DOUBLE], op=MPI.SUM)

        if rank == 0:
            print('Iteration number = ', s, ', error = ', np.sqrt(error)[0])

    return x_full


# Prepare data
if rank == 0:
    f1 = open('in.dat', 'r')
    N = np.array(np.int32(f1.readline()))
    M = np.array(np.int32(f1.readline()))
    f1.close()
else:
    N = np.array(0, dtype=np.int32)

comm.Bcast([N, 1, MPI.INT], root=0)

if rank == 0:
    rcounts_M, displs_M = aux_rcounts_and_displs(M, numprocs)
else:
    rcounts_M = None
    displs_M = None

M_part = np.array(0, dtype=np.int32)

comm.Scatter([rcounts_M, 1, MPI.INT], [M_part, 1, MPI.INT], root=0)

if rank == 0:
    f2 = open('AData.dat', 'r')
    for k in range(1, numprocs):
        A_part = np.empty((rcounts_M[k], N), dtype=np.float64)
        for i in range(rcounts_M[k]):
            for j in range(N):
                A_part[i, j] = float(f2.readline())
        comm.Send([A_part, rcounts_M[k] * N, MPI.DOUBLE], dest=k, tag=0)
    f2.close()
    A_part = np.empty((M_part, N), dtype=np.float64)
else:
    A_part = np.empty((M_part, N), dtype=np.float64)
    comm.Recv([A_part, M_part * N, MPI.DOUBLE], source=0, tag=0)

if rank == 0:
    b = np.empty(M, dtype=np.float64)
    f3 = open('bData.dat', 'r')
    for i in range(M):
        b[i] = float(f3.readline())
    f3.close()
else:
    b = None

b_part = np.empty(M_part, dtype=np.float64)

comm.Scatterv([b, rcounts_M, displs_M, MPI.DOUBLE], [b_part, M_part, MPI.DOUBLE], root=0)

x = np.zeros(N, dtype=np.float64)

x = conjugate_gradient_method(A_part, b_part, x, N)

if rank == 0:
    fig, ax = plt.subplots()
    plt.plot(x, color='green', lw=3)
    plt.xlabel(r'$i$')
    plt.ylabel(r'$x_i$')
    plt.show()
