import numpy as np
import matplotlib.pyplot as plt
from mpi4py import MPI

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()


def get_init_conditions(x_in, x_max_in, eps_in):
    if (x_in != 0) and (x_in != x_max_in):
        return 0.5 * np.tanh((x_in - 0.6) / eps_in)
    elif x_in == 0:
        return -0.5
    else:
        return 0.5


x_max = 1
t_max = 6
x_step = 0.01
t_step = 0.001

eps = 10 ** (-1.5)

x_part = np.linspace((rank * x_max / numprocs) // x_step * x_step,
                     (((rank + 1) * x_max / numprocs) // x_step + 1) * x_step,
                     int(((rank + 1) * x_max / numprocs) // x_step + 2 - (rank * x_max / numprocs) // x_step),
                     dtype=np.float64)
t_grid = np.arange(0, t_max + t_step, t_step)

u_part = np.zeros([x_part.size, t_grid.size], dtype=np.float64)
u_part[:, 0] = np.array([get_init_conditions(x, x_max, eps) for x in x_part], dtype=np.float64)

idx_r = (rank + 1) % numprocs
idx_l = (rank - 1) % numprocs

for n in range(0, t_grid.size - 1):
    for j in range(1, x_part.size - 1):
        u_part[j, n + 1] = \
            u_part[j, n] + eps * t_step / x_step ** 2 * (u_part[j + 1, n] - 2 * u_part[j, n] + u_part[j - 1, n]) + \
            t_step / (2 * x_step) * u_part[j, n] * (u_part[j + 1, n] - u_part[j - 1, n]) + t_step * u_part[j, n] ** 3

    u_part[0, n + 1] = comm.sendrecv(u_part[-2:-1, n + 1], dest=idx_r, source=idx_l)[0]
    u_part[-1, n + 1] = comm.sendrecv(u_part[1:2, n + 1], dest=idx_l, source=idx_r)[0]

    u_part[:, n + 1] *= (x_part != 0) * (x_part != x_max)
    u_part[:, n + 1] += -0.5 * (x_part == 0) + 0.5 * (x_part == x_max)

u = comm.gather(u_part)
x = comm.gather(x_part)

# Visualization of solution
if rank == 0:
    u = np.vstack(u)
    x = np.hstack(x)

    fig, ax = plt.subplots()
    for k, time in enumerate([0, 1, 2, 3, 4, 5, 6]):
        ax.plot(x, u[:, t_grid == time], color='blue', alpha=(1 / (k + 1)), label='t = ' + str(time))
        ax.set_xlabel('x')
        ax.set_ylabel('u')
        ax.legend()
    plt.show()
