from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

N = 16
num = int(np.sqrt(numprocs))

comm_row = comm.Split(rank // num, rank)
comm_col = comm.Split(rank % num, rank)

x_part = np.zeros(N // num, dtype=np.float64)
x = np.zeros(N, dtype=np.float64)

if rank == 0:
    x = np.random.randn(N)

comm_row.Scatter([x, N // num, MPI.DOUBLE], [x_part, N // num, MPI.DOUBLE], root=0)
comm_col.Bcast(x_part, root=0)

A_part = np.random.randn(N // num, N // num)

b_part_temp = np.dot(A_part, x_part)
b_part = np.zeros_like(x_part)
comm_row.Reduce(b_part_temp, b_part, op=MPI.SUM)

if comm_row.Get_rank() == 0:
    b = np.zeros(N, dtype=np.float64)
    comm_col.Gather([b_part, N // num, MPI.DOUBLE], [b, N // num, MPI.DOUBLE], root=0)

if rank == 0:
    print('b_MPI = ', b)

A = np.zeros((N, N), dtype=np.float64)
comm.Gather([A_part, (N // num) ** 2, MPI.DOUBLE], [A, (N // num) ** 2, MPI.DOUBLE], root=0)

# Check program
if rank == 0:
    print(A.shape)
    A = A.reshape((num, num, N // num, N // num))
    A = np.transpose(A, (0, 2, 1, 3))
    A = A.reshape((N, N))
    error = np.abs(np.dot(A, x) - b)
    for i in range(len(error)):
        print('Number of element of error:', i, ', Error: ', error[i])
