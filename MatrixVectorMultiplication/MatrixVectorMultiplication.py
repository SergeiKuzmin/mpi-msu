from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

if rank == 0:
    f1 = open('in.dat', 'r')
    N = np.array(np.int32(f1.readline()))
    M = np.array(np.int32(f1.readline()))
    f1.close()
else:
    N = np.array(0, dtype=np.int32)
    M = np.array(0, dtype=np.int32)
  
comm.Bcast([N, 1, MPI.INT], root=0)
comm.Bcast([M, 1, MPI.INT], root=0)

if rank == 0:
    f2 = open('AData.dat', 'r')
    for k in range(1, numprocs):
        A_part = np.empty((M // (numprocs-1), N), dtype=np.float64)
        for i in range(M // (numprocs-1)):
            for j in range(N):
                A_part[i, j] = float(f2.readline())
        comm.Send([A_part, (M // (numprocs-1)) * N, MPI.DOUBLE], dest=k, tag=0)
    f2.close()
else:
    A_part = np.empty((M // (numprocs-1), N), dtype=np.float64)
    comm.Recv([A_part, M // (numprocs-1)*N, MPI.DOUBLE], source=0, tag=0)

x = np.empty(N, dtype=np.float64)
if rank == 0:
    f3 = open('xData.dat', 'r')
    for i in range(N):
        x[i] = float(f3.readline())
    f3.close() 

comm.Bcast([x, N, MPI.DOUBLE], root=0)

b_part = np.empty(M // (numprocs - 1), dtype=np.float64)
b_part = np.dot(A_part, x)

if rank != 0:
    comm.Send([b_part, M // (numprocs-1), MPI.DOUBLE], dest=0, tag=0)

if rank == 0:
    b = np.empty(M, dtype=np.float64)
    b_part = np.empty(M // (numprocs - 1), dtype=np.float64)
    for k in range(1, numprocs):
        comm.Recv([b_part,  M // (numprocs - 1), MPI.DOUBLE], source=k, tag=0)
        b[(M // (numprocs - 1)) * (k - 1):(M // (numprocs - 1)) * k] = b_part
    print(b)
