from mpi4py import MPI
import numpy as np

comm = MPI.COMM_WORLD
numprocs = comm.Get_size()
rank = comm.Get_rank()

print('rank = ', rank)

# Prepare data
if rank == 0:
    f1 = open('in.dat', 'r')
    N = np.array(int(f1.readline()))
    M = np.array(int(f1.readline()))
    f1.close()
else:
    N = np.array(0, dtype=np.int32)
    M = np.array(0, dtype=np.int32)

comm.Bcast([N, 1, MPI.INT], root=0)
comm.Bcast([M, 1, MPI.INT], root=0)

if rank == 0:
    ave, res = divmod(M, numprocs-1)
    rcounts = np.empty(numprocs, dtype=np.int32)
    displs = np.empty(numprocs, dtype=np.int32)
    rcounts[0] = 0
    displs[0] = 0
    for k in range(1, numprocs):
        if k < 1 + res:
            rcounts[k] = ave + 1
        else:
            rcounts[k] = ave
        displs[k] = displs[k - 1] + rcounts[k - 1]
    print('rcounts = ', rcounts)
    print('displs = ', displs)
else:
    rcounts = None
    displs = None

M_part = np.array(0, dtype=np.int32)
comm.Scatter([rcounts, 1, MPI.INT], [M_part, 1, MPI.INT], root=0)

if rank == 0:
    f2 = open('AData.dat', 'r')
    for k in range(1, numprocs):
        A_part = np.zeros((rcounts[k], N), dtype=np.float64)
        for j in range(rcounts[k]):
            for n in range(N):
                A_part[j][n] = float(f2.readline())
        comm.Send([A_part, rcounts[k] * N, MPI.DOUBLE], dest=k, tag=0)
    A_part = np.zeros((M_part, N)).astype(dtype=np.float64)
    f2.close()
else:
    A_part = np.zeros((M_part, N), dtype=np.float64)
    comm.Recv([A_part, M_part * N, MPI.DOUBLE], source=0, tag=0)

x = np.zeros(N, dtype=np.float64)
if rank == 0:
    f3 = open('xData.dat', 'r')
    for i in range(N):
        x[i] = float(f3.readline())
    f3.close()

comm.Bcast([x, N, MPI.DOUBLE], root=0)

# Calculating AT_x = A^{T} * x
x_part = np.zeros(M_part, dtype=np.float64)

comm.Scatterv([x, rcounts, displs, MPI.DOUBLE], [x_part, M_part, MPI.DOUBLE])

AT_x_part = np.dot(A_part.T, x_part)

if rank == 0:
    AT_x_part = np.zeros(N, dtype=np.float64)
    AT_x = np.zeros(N, dtype=np.float64)
else:
    AT_x = None

comm.Reduce([AT_x_part, N, MPI.DOUBLE], [AT_x, N, MPI.DOUBLE], root=0, op=MPI.SUM)

# Answer with using MPI
if rank == 0:
    print('A^{T} * x = ', AT_x)

# Check program
if rank == 0:
    f1 = open('in.dat', 'r')
    N = np.array(int(f1.readline()))
    M = np.array(int(f1.readline()))
    f1.close()

    print('N = ', N)
    print('M = ', M)

    f2 = open('AData.dat', 'r')
    A = np.zeros((M, N), dtype=np.float64)
    for i in range(M):
        for j in range(N):
            A[i][j] = float(f2.readline())
    f2.close()

    x = np.zeros(N, dtype=np.float64)
    f3 = open('xData.dat', 'r')
    for i in range(N):
        x[i] = float(f3.readline())
    f3.close()

    print('Check: A^{T} * x = ', np.dot(A.T, x))
